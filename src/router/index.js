import Vue from 'vue'
import VueRouter from 'vue-router'
import Detail from '../component/Detail.vue'
import Input from '../component/Input.vue'

Vue.use(VueRouter)

const routes = [
  {
    name: 'input',
    path: '',
    component: Input,
  },
  {
    name: 'detail',
    path: '/detail/:id',
    component: Detail,
  },
]

export default routes