import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

import App from './App.vue'
import store from './store/store.js'
import routes from './router/index'

import 'bulma/css/bulma.css'
import '../index.css'

const router = new VueRouter({
  mode: 'history',
  base: __dirname,
  routes,
})

new Vue({
  el: '#app',
  store,
  router,
  render: h => h(App)
})
