const ADD_NOTE = 'addNote'
const DETAIL_NOTE = 'detailNote'
const DELETE_NOTE = 'deleteNote'
const UPDATE_NOTE = 'updateNote'

import Vue from 'vue'
import Vuex from 'vuex'


Vue.use(Vuex)
export default new Vuex.Store({
  state: {
    notes: [
      {
        title: 'Hi There',
        text: 'You are welcome',
        date: new Date(Date.now()).toLocaleString(),
      },
      {
        title: 'You are awesome',
        text: 'Thank you',
        date: new Date(Date.now()).toLocaleString(),
      },
    ]
  },
  mutations: {
    [ADD_NOTE] (state, payload) {
      state.notes.push(payload)
    },
    [DETAIL_NOTE] (state, index) {
      state.notes.push(index, 1)
    },
    [DELETE_NOTE] (state, index){
      state.notes.splice(index, 1)
    },
    [UPDATE_NOTE] (state, { index, newData }){
      if (newData.text) state.notes[index].text = newData.text
      if (newData.title) state.notes[index].title = newData.title
    }
  },
  actions: {
    addNote ({commit}, note){
      console.log(note)
      commit(ADD_NOTE, note)
    },
    detailNote({commit}, index){
      commit(DETAIL_NOTE, index)
    },
    deleteNote({commit}, index){
      commit(DELETE_NOTE, index)
    },
    updateNote({commit}, { index, newData }){
      commit(UPDATE_NOTE, { index, newData })
    }
  },
  getters: {
    filterNoteById: (state) => (id) => {
      return state.notes.filter((item, index) => {
        return index == id
      })
    }
  }
})
